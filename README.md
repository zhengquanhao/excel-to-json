Excel转JSON插件
=======

### 放入需要转换的Excel文件

将`translate.xlsx`放入到`excel`目录下，也可以在`translate.js`自定义配置

```javascript
let sheets = xlsx.parse(__dirname + '/excel/translate.xlsx');
```

### 将Excel各列与各语言对应

`translate.js`:
```javascript
const rowIndex = {
	en: 2, // 英文列索引，将作为en.json的值
	zh: 1, // 中文列索引，将作为zh.json的值
	id: 3, // 印尼文索引，将作为id.json的值
	key: 2, // 将英文的作为json的键
}
```

### 启动项目

```shell
cd translateX
npm i
node translate.js
```

### 说明

换行符、引号将被过滤成空格
